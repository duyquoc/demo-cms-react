import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Layout, Icon, Menu} from 'antd';
import './styles.scss';

export default class Header extends Component {

    static contextTypes = {
        collapsed: PropTypes.bool.isRequired
    };

    static propsTypes = {
      toggle: PropTypes.func.isRequired
    };

    render() {
        const
            {collapsed} = this.context,
            {toggle} = this.props;

        const avatarUrl = "https://fakeimg.pl/350x350/?retina=1&text=Avatar";

        return (
            <Layout.Header className="headerComponent">
                <Icon
                    className="trigger"
                    type={collapsed ? 'menu-unfold' : 'menu-fold'}
                    onClick={toggle}
                />
                <div>
                    <Menu mode="horizontal" onClick={() => {}}>
                        <Menu.SubMenu className="langLink" title={
                            <span>
                                <i className="flag-icon flag-icon-gb" /> <Icon type="down" />
                            </span>
                        }>
                            <Menu.Item key="en"><i className="flag-icon flag-icon-gb" /> English</Menu.Item>
                            <Menu.Item key="fr"><i className="flag-icon flag-icon-fr" /> Français</Menu.Item>
                        </Menu.SubMenu>
                        <Menu.SubMenu className="userLink" title={
                            <span>
                                <img src={avatarUrl} alt="avatar" /> John Doe <Icon type="down" />
                            </span>
                        }>
                            <Menu.Item key="editProfile"><Icon type="user"/> Edit profile</Menu.Item>
                            <Menu.Item key="logout"><Icon type="logout"/> Logout</Menu.Item>
                        </Menu.SubMenu>
                    </Menu>
                </div>
            </Layout.Header>
        );
    }
}