import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Layout} from 'antd';
import Sider from './sider';
import Header from './header';
import './styles.scss';

export default class LayoutAdmin extends Component {

    static childContextTypes = {
        collapsed: PropTypes.bool.isRequired
    };

    getChildContext() {
        return {collapsed: this.state.collapsed};
    }

    state = {
        collapsed: false
    };

    toggle = () => {;
        this.setState({
            collapsed: !this.state.collapsed
        });
    };

    render() {
        return (
            <Layout id="layoutAdminComponent" className="ant-layout-has-sider">
                <Sider />
                <Layout>
                    <Header toggle={this.toggle} />
                    <Layout.Content>
                        Content
                    </Layout.Content>
                </Layout>
            </Layout>
        )
    }
}