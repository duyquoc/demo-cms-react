import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Layout, Menu, Icon} from 'antd';
import './styles.scss';

export default class Sider extends Component {

    static contextTypes = {
        collapsed: PropTypes.bool.isRequired
    };

    render() {
        const {collapsed} = this.context;

        const menu = {
            current: 'dashboard',
            items: {
                dashboard: {
                    icon: 'home',
                    label: 'Dashboard'
                },
                users: {
                    icon: 'user',
                    label: 'Users'
                },
                acls: {
                    icon: 'user',
                    label: 'Acls'
                }
            }
        };

        return (
            <Layout.Sider
                trigger={null}
                collapsible
                collapsed={collapsed}
                className="siderComponent"
            >
                <div className="logo">
                    <img src={require('assets/images/logo-light.png')} alt="" />
                    {collapsed === false && 'CMS'}
                </div>
                <Menu theme="dark" mode="inline" defaultSelectedKeys={menu.current}>
                    {
                        Object.keys(menu.items).map((key) => {
                            const item = menu.items[key];
                            return (
                                <Menu.Item key={key}>
                                    <Icon type={item.icon} />
                                    <span className="nav-text">{item.label}</span>
                                </Menu.Item>
                            );
                        })
                    }
                </Menu>
            </Layout.Sider>
        );
    }
}