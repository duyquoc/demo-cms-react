import {applyMiddleware, compose, createStore} from 'redux';
import {persistStore, autoRehydrate} from 'redux-persist';
import thunk from 'redux-thunk';
import client from './apolloClient';
import reducers from './reducers';

export default (initialState = {}) => {
    let middleware = applyMiddleware(thunk);
    const middlewares = [thunk, client.middleware()];

    if (process.env.NODE_ENV !== 'production') {
        // configure redux-devtools-extension
        // @see https://github.com/zalmoxisus/redux-devtools-extension
        const devToolsExtension = window.devToolsExtension;
        if (typeof devToolsExtension === 'function') {
            middleware = compose(applyMiddleware(...middlewares), devToolsExtension(), autoRehydrate());
        }
    } else {
        middleware = compose(middleware, autoRehydrate());
    }

    const store = createStore(reducers, initialState, middleware);
    persistStore(store);

    return store;
};