import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';
import client from './apolloClient';
import {authReducer} from './auth';

export default combineReducers({
    routing: routerReducer,
    apollo: client.reducer(),
    auth: authReducer
});