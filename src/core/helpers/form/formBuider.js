import React from 'react';
import {Form} from 'antd';
import Fields from './fields';

export default class FormBuilder {

    form = {};
    callbackSubmit;
    fields = {};

    /**
     * Constructor
     *
     * @param form
     */
    constructor(form) {
        this.form = form;
    }

    /**
     * Set callback submit
     *
     * @param callbackSubmit
     * @returns {FormBuilder}
     */
    setCallbackSubmit(callbackSubmit) {
        this.callbackSubmit = callbackSubmit;
        return this;
    }

    /**
     * Handle submit
     *
     * @param e
     */
    handleSubmit = (e) => {
        e.preventDefault();
        this.form.validateFields((err, values) => {
            if (!err) {
                this.callbackSubmit(values);
            }
        });
    };

    /**
     * Add field
     *
     * @param name
     * @param FieldType
     * @param options
     * @returns {FormBuilder}
     */
    add(name, FieldType = Fields.InputType, options) {
        this.fields[name] = <FieldType name={name} options={options} />;
        return this;
    }

    /**
     * Render form
     *
     * @returns {XML}
     */
    render() {
        return (
            <Form onSubmit={this.handleSubmit}>
                {this.renderFields()}
            </Form>
        );
    }

    /**
     * Render fields
     *
     * @returns {Array}
     */
    renderFields() {
        return Object.keys(this.fields).map((name) => {
            return React.cloneElement(this.fields[name], {key: name});
        });
    }
}