import {lowerFirst} from 'lodash';

export const getRulesByOptions = (options) => {
    if (!options.validators) return [];

    return options.validators.map((validator) => {
        if (validator.required && !validator.message && (options.label || options.placeholder)) {
            validator.message = `Please input your ${lowerFirst(options.label ? options.label : options.placeholder)} !`;
        }
        return validator;
    });
};