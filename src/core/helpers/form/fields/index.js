import InputType from './inputType';
import ButtonType from './buttonType';
import CheckboxType from './checkboxType';

export default {
    InputType,
    ButtonType,
    CheckboxType
};