import React from 'react';
import PropTypes from 'prop-types';
import {pick} from 'lodash';
import {Form, Input, Icon} from 'antd';
import {getRulesByOptions} from '../utils';

const InputType = ({name, options}, context) => {
    let optionsAllowed = pick(options, ['type', 'placeholder', 'prefix']);

    if (options.icon) {
        optionsAllowed['prefix'] = <Icon type={options.icon} style={{ fontSize: 13 }} />;
    }

    return (
        <Form.Item label={options.label} hasFeedback>
            {context.form.getFieldDecorator(name, {rules: getRulesByOptions(options)})(
                <Input {...optionsAllowed} />
            )}
        </Form.Item>
    );
};
InputType.propTypes = {
    name: PropTypes.string.isRequired
};
InputType.contextTypes = {
    form: PropTypes.object
};

export default InputType;