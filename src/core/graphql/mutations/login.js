import gql from 'graphql-tag';

const LOGIN_MUTATION = gql`
    mutation login($usernameOrEmail: String!, $password: String!) {
        login(usernameOrEmail: $usernameOrEmail, password: $password) {
            id
            jwt
        }
    }
`;

export default LOGIN_MUTATION;