import apolloClient from 'core/apolloClient';
import {SET_CURRENT_USER, LOGOUT} from './action-types';

export const setCurrentUser = user => ({
    type: SET_CURRENT_USER,
    user
});

export const logout = () => {
    apolloClient.resetStore();
    return {type: LOGOUT};
};