import Login from './login';
import Register from './register';
import ForgotPassword from './forgotPassword';
import './styles.scss';

export {
    Login,
    Register,
    ForgotPassword
}