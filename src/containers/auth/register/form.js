import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Form} from 'antd';
import {FormBuilder, Fields} from 'core/helpers/form/index';

class RegisterForm extends Component {

    static propTypes = {
        handleSubmit: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.formBuilder = new FormBuilder(props.form);
        this.formBuilder
            .setCallbackSubmit(props.handleSubmit)
            .add('username', Fields.InputType, {
                placeholder: 'Username',
                icon: 'user',
                validators: [{
                    required: true
                }]
            })
            .add('email', Fields.InputType, {
                placeholder: 'Email',
                icon: 'mail',
                validators: [{
                    required: true
                }]
            })
            .add('password', Fields.InputType, {
                placeholder: 'Password',
                type: 'password',
                icon: 'lock',
                validators: [{
                    required: true
                }]
            })
            .add('confirm_password', Fields.InputType, {
                placeholder: 'Confirm password',
                type: 'password',
                icon: 'lock',
                validators: [{
                    required: true
                }]
            })
            .add('submit', Fields.ButtonType, {
                label: 'Create my account'
            })
    }

    render() {
        const {children}  = this.props;
        return (
            <Form onSubmit={this.formBuilder.handleSubmit}>
                <div className="logo">
                    <img src={require('assets/images/logo-dark.png')} alt="" /> CMS
                </div>
                <p className="title">Create an account</p>
                {this.formBuilder.renderFields()}
                {children}
            </Form>
        );
    }
}

export default Form.create()(RegisterForm);