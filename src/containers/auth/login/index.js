import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {graphql} from 'react-apollo';
import {Link} from 'react-router';
import {setCurrentUser} from 'core/auth/actions';
import LOGIN_MUTATION from 'core/graphql/mutations/login';
import {message} from 'antd';
import LoginForm from './form';
import {paths} from 'router';
import './styles.scss';

@graphql(LOGIN_MUTATION, {
    props: ({mutate}) => ({
        login: ({usernameOrEmail, password}) =>
            mutate({
                variables: {usernameOrEmail, password}
            }),
    })
})
export default class Login extends Component {

    static contextTypes = {
        router: PropTypes.object
    };

    state = {
        loading: false
    };

    handleSubmit = ({usernameOrEmail, password}) => {
        const {dispatch, login} = this.props;

        this.setState({loading: true});
        login({usernameOrEmail, password})
            .then(({data: {login: user}}) => {
                dispatch(setCurrentUser(user));
                this.setState({loading: false});
                this.context.router.push(paths.ADMIN);
            })
            .catch(() => {
                this.setState({loading: false});
                message.error('This is a message of error');
            });
    };

    render() {
        return (
            <div id="loginContainer" className="authContainer">
                <LoginForm handleSubmit={this.handleSubmit}>
                    <div className="blockBetween">
                        <Link to={paths.FORGOT_PASSWORD}>Forgot password ?</Link>
                        <Link to={paths.REGISTER}>Sign Up</Link>
                    </div>
                </LoginForm>
            </div>
        );
    }
}