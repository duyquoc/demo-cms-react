import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Form} from 'antd';
import {FormBuilder, Fields} from 'core/helpers/form/index';

class LoginForm extends Component {

    static propTypes = {
        handleSubmit: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.formBuilder = new FormBuilder(props.form);
        this.formBuilder
            .setCallbackSubmit(props.handleSubmit)
            .add('email', Fields.InputType, {
                placeholder: 'Your email',
                icon: 'mail',
                validators: [{
                    required: true
                }]
            })
            .add('submit', Fields.ButtonType, {
                label: 'Recover password'
            })
    }

    render() {
        const {children} = this.props;
        return (
            <Form onSubmit={this.formBuilder.handleSubmit}>
                <div className="logo">
                    <img src={require('assets/images/logo-dark.png')} alt="" /> CMS
                </div>
                <p className="title">Forgot your password</p>
                <p>We will send you a link to reset password.</p>
                {this.formBuilder.renderFields()}
                {children}
            </Form>
        );
    }
}

export default Form.create()(LoginForm);