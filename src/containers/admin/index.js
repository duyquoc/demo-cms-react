import React from 'react';
import LayoutAdmin from 'components/layoutAdmin';

export default ({children}) => {
    return (
        <LayoutAdmin>
            {children}
        </LayoutAdmin>
    );
}